<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Tokoku</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
 
    <!-- Styles -->
    <link href="{{ asset('css/theme5.min.css') }}" rel='stylesheet' type='text/css'>
    <link href="{{ asset('css/sweetalert.css') }}" rel='stylesheet' type='text/css'>
    <link href="{{ asset('css/selectize.bootstrap3.css') }}" rel='stylesheet' type='text/css'>
    {{-- <link href="{{ asset('css/font-awesome.min.css') }}"  rel='stylesheet' type='text/css'> --}}
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }

        /* Add animation to "page content" */
        .animate-bottom {
          position: relative;
          -webkit-animation-name: animatebottom;
          -webkit-animation-duration: 1s;
          animation-name: animatebottom;
          animation-duration: 1s
        }

        @-webkit-keyframes animatebottom {
          from { bottom:-100px; opacity:0 } 
          to { bottom:0px; opacity:1 }
        }

        @keyframes animatebottom { 
          from{ bottom:-100px; opacity:0 } 
          to{ bottom:0; opacity:1 }
        }

        .table>tbody>tr>td, . table>tfoot>tr>td{
            vertical-align: middle;
        }

        @media screen and (max-width: 600px) {
            table.cart tbody td . form-control{
                width:20% ;
                display: inline !important;
            }
            .actions .btn{
                width:36% ;
                margin: 1.5em 0;
            }
            .actions .btn-info{
                float:left;
            }
            .actions .btn-danger{
                float:right;
            }

            table.cart thead { display: none; }
            table.cart tbody td { display: block; padding: .6rem; min-width:320px;}

            table.cart tbody tr td:first-child { background: #333; color: #fff;}
            table.cart tbody td:before {
                content: attr(data- th); font-weight: bold;
                display: inline- block; width: 8rem;
            }
            table.cart tfoot td{display: block; }
            table.cart tfoot td .btn{display:block;}
        }
        .form-inline {
            display: inline;
        }
    </style>
</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
 
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    Tokoku
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                @if(Auth::check())
                    <li><a href="{{ url('/home') }}">Home</a></li>
                    @if (Auth::user()->role == 'admin')
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                Manage <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ route('categories.index') }}"><i class="fa fa-btn fa-tags"></i>Categories</a></li>
                                <li><a href="{{ route('products.index') }}"><i class="fa fa-btn fa-tags"></i>Products</a></li> 
                                <li><a href="{{ route('orders.index') }}"><i class="fa fa-btn fa-shopping-cart"></i>Orders</a></li>

                            </ul>
                        </li>
                    @elseif (Auth::user()->role == 'customer')
                        <li><a href="{{ url('/order-history') }}">Order History</a></li>
                    @endif
                @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @include('layouts.customer-feature', ['partial_view' => 'layouts.cart-menu'])
                    
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @include('layouts.flash')
    <div class="animate-bottom">
        @yield('content')
    </div>
    @include('vendor.sweet.alert')

    <!-- JavaScripts --> 
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    @include('sweet::alert')
    @if(Session::has('flash_product'))
        @include('catalogs.product-added', ['product_name' => Session::get('flash_product')])
    @endif
    <script src="{{ asset('js/selectize.min.js') }}"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}} 
</body>
</html>
 