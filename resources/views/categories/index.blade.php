@extends('layouts.app')
    @section('content')
    <div class= "container">
        <div class= "row">
            <div class= "col-md-12">
                <h3> Category  <a href= "{{ route('categories.create') }}" class= "btn btn-danger btn-sm" >+</a></h3>
                <div class= "col-md-4">
                    {!! Form::open(['url' => 'categories', 'method'=>'get', 'class'=>'form-inline']) !!}

                        <div class="form-group {!! $errors->has('query') ? 'has-error' : '' !!}">
                        {!! Form::text('query', isset($query) ? $query : null, ['class'=>'form-control','placeholder' => 'Searching ...']) !!}
                        {!! $errors->first('query', '<p class="help-block">:message</p>') !!}
                        </div>

                        {!! Form::submit('Search', ['class'=>'btn btn-primary']) !!}

                    {!! Form::close() !!}
                 </div>

                <table class= "table table-hover">
                    <thead>
                        <tr>
                            <td>Title</td>
                            <td>Parent</td>
                        </tr> 
                    </thead>
                    <tbody>
                        @foreach( $categories as $category)
                        <tr>
                            <td>{{ $category-> title }} </td>
                            <td>{{ $category-> parent ? $category->parent->title : '' }}</td>
                            <td>
                                {!! Form::model($category, ['route' => ['categories.destroy', $category], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                <a href = "{{ route('categories.edit', $category->id)}}" class = "btn btn-xs btn-success">Edit</a> |
                                {!! Form::submit('delete', ['class'=>'btn btn-xs btn-danger js-submit-confirm']) !!}
                                {!! Form::close() !!}
                            </td> 
                        </tr>
                        @endforeach
                    </body>
                </table>
                {{ $categories->appends(compact('query'))->links() }}
            </div>
        </div>
    </div>
@endsection