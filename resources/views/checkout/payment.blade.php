@extends('layouts.app')

@section('content')
<div class="container">
	@include('checkout.step')
	<div class="row">
		<div class="col-md-8">
			<div class="panel panel-default">
				<div class="panel-heading">Pembayaran</div>
				<div class="panel-body">
					@include('checkout.payment-form')
				</div>
			</div>
		</div>
		<div class="col-md-4">
			@include('checkout.cart-panel')
		</div>
	</div>
</div>
@endsection