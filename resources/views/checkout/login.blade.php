@extends('layouts/app')

@section('content')
	<div class="container">
		@include('checkout.step')
		<div class="row">
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-heading">Login atau Checkout tanpa mendaftar</div>
					<div class="panel-body">
					@if(session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif	
						@include('checkout.login-form')
					</div>
				</div>
			</div>
			<div class="col-md-4">
				@include('checkout.cart-panel') 
			</div>
		</div>
	</div>	
@endsection