@extends('layouts.app')

@section('content')
<div class="container">
  @include('checkout.step')
  <div class="row">
    <div class="col-md-8">
      <div class="panel panel-default">
          <div class="panel-heading">Alamat Pengiriman</div>
          <div class="panel-body">
            @if (auth()->check())
              @include('checkout.address-choose')
            @else
              @include('checkout.address-form')
            @endif
          </div> 
      </div>
    </div>
    <div class="col-md-4">
      @include('checkout.cart-panel')
    </div>
  </div>
</div>
@endsection