@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading"><h3>Order History</h3></div>
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table">
                <thead> 
                  <tr>
                    <th style="width:10%">Padded ID</th>
                    <th style="width:25%">Address</th>
                    <th style="width:8%">Bank</th>
                    <th style="width:8%">Sender</th>
                    <th style="width:12%">Total Payment</th>
                    <th style="width:10%">Status</th>
                    <th style="width:10%">Details</th>
                  </tr>
                </thead>
                <tbody>
                @forelse ($orders as $order)
                    <tr>
                      <td>{{ $order->padded_id }}</td>
                      <td>
                        <address>
                         <strong>{{ $order->address->name }}</strong> <br>
                         {{ $order->address->detail }} <br>
                         {{ $order->address->regency->name }}, {{ $order->address->regency->province->name }} <br>
                         <abbr title="Phone">P:</abbr>  +62 {{ $order->address->phone }}
                        </address>
                      </td>
                      <td>{{ config('bank')[$order->bank]['bank'] }}</td>
                      <td>{{ $order->sender }}</td>
                      <td>{{ $order->total_payment }}</td>
                      <td>
                        @if($order->status == "waiting-payment")
                          <span class="label label-default">{{ $order->human_status }}</span>
                        @elseif($order->status == "packaging")
                          <span class="label label-info">{{ $order->human_status }}</span>
                        @elseif($order->status == "sent")
                          <span class="label label-primary">{{ $order->human_status }}</span>
                         @elseif($order->status == "finished")
                          <span class="label label-success">{{ $order->human_status }}</span>
                        @endif 
                      </td>
                      <td>
                       <a href="{{ url('order-history-detail', $order->id) }}" class="btn btn-danger" aria-label="Left Align">
                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                      </a>
                      </td>
                    </tr>
                @empty
                    <tr>
                      <td colspan ="7" class="text-center">
                          <h1>:(</h1>
                          <p>Order kamu masih kosong</p>
                          <p><a href="{{ url('/catalogs') }}">Yuk, belanja di Tokoku<i class="fa fa-arrow-right"></i></a></p>
                        </div>
                      </td>
                    </tr>
                @endforelse
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> 
@endsection