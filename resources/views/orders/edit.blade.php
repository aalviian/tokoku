@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading"><h3>Edit {{ $order->title }}</h3></div>
          <div class="panel-body">
            {!! Form::model($order, ['route' => ['orders.update', $order], 'method'=>'patch', 'class'=>'form-horizontal'])!!}
            <div class="col-md-3"></div>
            <div class="col-md-7">
              @include('orders.form', ['model' => $order])
            </div>
            <div class="col-md-2"></div>
            {!! Form::close() !!} 
          </div>
        </div>
      </div>
    </div>
  </div> 
@endsection