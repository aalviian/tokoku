@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading"><h3>Order History <small>Padded ID : <strong>{{ $order->padded_id }}</strong></small></h3></div>
          <div class="panel-body">
              <table class="cart table table-hover table-condensed">
                <thead> 
                  <tr>
                    <th style="width:40%">Product</th>
                    <th style="width:10%" class="text-center">Quantity</th>
                    <th style="width:15%" class="text-center">Price</th>
                    <th style="width:15%" class="text-center">Fee</th>
                    <th style="width:15%" class="text-center">Total Price</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($details as $detail)
                    <tr>
                      <td>
                        <div class="row">
                            <div class="col-md-2"> 
                              <img src="{{ $detail->product->photo_path }}" alt="..." width="40" height="40" class="img-responsive">
                            </div>
                            <div class="col-md-10">
                              {{ $detail->product->name }}
                            </div>
                        </div>
                        <br>
                      </td>
                      <td class="text-center">
                        {{ $detail->quantity }}
                      </td>
                      <td class="text-center">
                        {{ $detail->price }}
                      </td>
                      <td class="text-center">
                        {{ $detail->fee }}
                      </td>
                      <td class="text-center">
                        {{ $detail->total_price }}
                      </td>
                    </tr>
                @endforeach
                </tbody>
              </table>
          </div>
        </div>
        <a href="{{ url('order-history') }}" class="btn btn-primary"><i class="glyphicon glyphicon-chevron-left">Back</i></a>
      </div>
    </div>
  </div> 
@endsection