@extends('layouts.app')
@section('content')
    <div class= "container">
        <div class= "row">
            <div class= "col-md-12">
                <h3> Product  <a href= "{{ route('products.create') }}" class= "btn btn-danger btn-sm" >+</a></h3>
                <div class= "col-md-4">
                    {!! Form::open(['url' => 'products', 'method'=>'get', 'class'=>'form-inline']) !!}

                        <div class="form-group {!! $errors->has('query') ? 'has-error' : '' !!}">
                        {!! Form::text('query', isset($query) ? $query : null, ['class'=>'form-control','placeholder' => 'Searching ...']) !!}
                        {!! $errors->first('query', '<p class="help-block">:message</p>') !!}
                        </div>

                        {!! Form::submit('Search', ['class'=>'btn btn-primary']) !!}

                    {!! Form::close() !!}
                 </div>

                <table class= "table table-hover">
                    <thead>
                        <tr>
                            <td>Name</td>
                            <td>Model</td>
                            <td>Category</td>
                            <td></td>
                        </tr> 
                    </thead>
                    <tbody>
                        @foreach( $products as $product)
                        <tr>
                            <td>{{ $product -> name }} </td>
                            <td>{{ $product -> model }}</td>
                            <td>
                                @foreach ($product->categories as $category)
                                <span class="label label-primary"><i class="fa fa-btn fa-tags"></i>{{ $category->title }} </span>
                                @endforeach
                            </td>
                            <td>
                                {!! Form::model($product, ['route' => ['products.destroy', $product], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                <a href = "{{ route('products.edit', $product->id)}}" class = "btn btn-xs btn-success">Edit</a> |
                                {!! Form::submit('delete', ['class'=>'btn btn-xs btn-danger js-submit-confirm']) !!}
                                {!! Form::close() !!}
                            </td> 
                        </tr>
                        @endforeach
                    </body>
                </table>
                {{ $products->appends(compact('query'))->links() }}
            </div>
        </div>
    </div>
@endsection