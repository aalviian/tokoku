<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Pencarian Produk</h3>
	</div>
	<div class="panel-body">
	{!! Form::open(['url'=>'catalogs', 'method'=>'get']) !!}
		<div class="form-group {!! $errors->has('query') ? 'has-error' : '' !!}">
			{!! Form::label('query', 'Apa yang kamu cari?') !!}
			{!! Form::text('query', $query, ['class'=>'form-control']) !!}
			{!! $errors->first('query', '<p class="help-block">:message</p>') !!}
		</div>

		{!! Form::hidden('id_category', $id_category) !!}

		{!! Form::submit('Cari', ['class'=>'btn btn-primary']) !!}
	{!! Form::close() !!}
	</div>
</div>