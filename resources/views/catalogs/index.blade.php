@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-3">
			@include('catalogs.search-panel', [
				'query' => isset($query) ? $query : null,
				'id_category' => isset($id_category) ? $id_category : ''
			])

			@include('catalogs.category-panel')

			@if (isset($category) && $category->hasChild())
			 	@include('catalogs.sub-category-panel', ['current_category' => $category])
			@endif

			@if (isset($category) && $category->hasParent())
				@include('catalogs.sub-category-panel', ['current_category' => $category->parent ])
			@endif 
		</div>
		<div class="col-md-9"> 
			<div class="row">
				<div class="col-md-12">
					@include('catalogs.breadcrumb', [
						'current_category' => isset($category) ? $category : null
					])

					@if($errors->has('quantity'))
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times:</button>
							{{ $errors->first('quantity') }}
						</div>
					@endif
				</div>
				@forelse($products as $product)
				<div class="col-md-6">
					@include('catalogs.product-thumbnail', ['product' => $product])
				</div>
				@empty
				<div class="col-md-12 text-center">
					@if(isset($query))
						<h1>:(</h1>
						<p>Produk yang kamu cari tidak ditemukan</p>
						@if(isset($category))
							<p><a href="{{ url('/catalogs?query=' . $query) }}">Cari di semua kategori <i class="fa fa-arrow-right"></i></a></p>
						@endif
					@else
						<h1>:(</h1>
						<p>Belum ada produk untuk kategori ini</p>
					@endif
					<p><a href="{{ url('/catalogs') }}">Lihat semua produk<i class="fa fa-arrow-right"></i></a></p>
				</div>
				@endforelse
			</div>
			<div class="pull-right">
				{!! $products->appends(compact('id_category', 'query', 'sort', 'order'))->links() !!}
			</div>
		</div>
	</div>
</div>
@endsection