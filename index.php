<!DOCTYPE html>
    <!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Maisipi | Coming Soon</title>
        
        <!-- Vendor CSS -->
        <link href="index/css/animate.min.css" rel="stylesheet">
        <link href="index/css/material-design-iconic-font.min.css" rel="stylesheet">
            
        <!-- CSS -->
        <link href="index/css/app.min.1.css" rel="stylesheet">
        <link href="index/css/app.min.2.css" rel="stylesheet">
    </head>
    
    <body>
    <div class="container">
        <div class="four-zero">
            <div class="fz-inner">
                <h3>Stay Tuned!</h3>
                <p>You are so curious..</p>
            </div>
        </div>
    </div>
    </body>
</html>