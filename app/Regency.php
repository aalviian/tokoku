<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use RajaOngkir;

class Regency extends Model
{
    protected $fillable = [
    	'id', 'province_id', 'name'
    ];

    public static function populate(){
    	foreach(RajaOngkir::Kota()->all() as $city){
    		$model = static::firstOrNew(['id' => $city['city_id']]);
    		$model->province_id = $city['province_id'];
    		$model->name = $city['type'] . ' ' . $city['city_name'];
    		$model->save();
    	}
    }

    public function province(){
    	return $this->belongsTo('App\Province');
    }
}
