<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Support\CartService;
use App\Http\Requests;
use App\Product;
use App\Cart;
use Validator;
use Session;
use Auth;

class CartController extends Controller
{
	protected $cart;

	public function __construct(CartService $cart){
		$this->cart = $cart;
	}

	public function show(){
		return view('carts.index');
	}

    public function addProduct(Request $request){
		$rules = [
            'product_id' => 'required|exists:products,id',
            'quantity' => 'required|integer|min:1'
        ];

        $messages = [
            'required' => 'Field harus di isi alias tidak boleh kosong',
            'exists' => 'Nama '.$request -> name.' tidak ada dalam database.',
            'integer' => 'Jumlah order harus lebih dari sama dengan 1',
            'min' => 'Jumlah order harus lebih dari sama dengan 1',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $product = Product::find($request->get('product_id'));
        $quantity = $request->get('quantity');
        Session::flash('flash_product', $product->name);

        if (Auth::check()){
        	$cart = Cart::firstOrCreate([
        		'product_id' => $product->id,
        		'user_id' => $request->user()->id
        	]);
        	$cart->quantity += $quantity;
        	$cart->save();
        	return redirect('catalogs');
        }

        else{
			$cart = $request->cookie('cart', []);
	        if(array_key_exists($product->id, $cart)){
	        	$quantity += $cart[$product->id];
	        }
	        $cart[$product->id] = $quantity;
	        return redirect('catalogs')->withCookie(cookie()->forever('cart', $cart));
        }
    }

    public function removeProduct(Request $request, $product_id){
    	$cart = $this->cart->find($product_id);
    	if(!$cart) return redirect('cart');

    	Session::flash('flash_notification', ["level"=>"success", "message"=>"Produk ".$cart['detail']['name']." berhasil dihapus dari cart"]);

    	if(Auth::check()){
        	$cart = Cart::firstOrCreate([
        		'user_id' => $request->user()->id,
        		'product_id' => $product_id
			]);
			$cart->delete();
			return redirect('cart');
        }
        else{
			$cart = $request->cookie('cart', []);
	    	unset($cart[$product_id]);
	    	return redirect('cart')->withCookie(cookie()->forever('cart', $cart));
        }
    }

    public function changeQuantity(Request $request, $product_id){
    	$rules = [
            'quantity' => 'required|integer|min:1'
        ];

        $messages = [
            'required' => 'Field harus di isi alias tidak boleh kosong',
            'integer' => 'Jumlah order harus lebih dari sama dengan 1',
            'min' => 'Jumlah order harus lebih dari sama dengan 1',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $quantity = $request->get('quantity');
        $cart = $this->cart->find($product_id);
        if(!$cart) return redirect('cart');

        if(Auth::check()){
        	$cart = Cart::firstOrCreate([
        		'user_id' => $request->user()->id,
        		'product_id' => $product_id
			]);
			$cart->quantity = $quantity;
			$cart->save();

			Session::flash('flash_notification', ["level"=>"success", "message"=>"Jumlah produk ".$cart->product->name." berhasil diubah menjadi ".$cart['quantity']." quantity"]);

			return redirect('cart');
        }
        else{
	        $cart = $request->cookie('cart', []);
	        $cart[$product_id] = $quantity;

	        Session::flash('flash_notification', ["level" =>"success", "message" => "Jumlah produk berhasil diubah menjadi ".$cart[$product_id]." quantity"]);

	        return redirect('cart')->withCookie(cookie()->forever('cart', $cart));
        }
    }
}
