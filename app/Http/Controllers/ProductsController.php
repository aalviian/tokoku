<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Http\Requests;
use App\Product;
use Validator;
use Session;
use File;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {
        $query = $request->get('query');
        $products = Product::where('name', 'LIKE', '%'.$query.'%') -> orWhere('model', 'LIKE', '%'.$query.'%') -> orderBy('name') -> paginate(5);

        return view('products.index', compact('products','query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:products',
            'model' => 'required',
            'photo' => 'mimes:jpeg,jpg,bmp,png|max:10240',
            'price' => 'required|numeric|min:1000',
            'weight' => 'required|numeric|min:1'
        ];

        $messages = [
            'required' => 'Field harus di isi alias tidak boleh kosong',
            'unique' => 'Nama '.$request -> name.' sudah ada dalam database.',
            'max' => 'Ukuran photo maksimal 10 MB ',
            'mimes' => 'Photo harus berekstensi JPG, JPEG, BMP, atau PNG',
            'numeric' => 'Field harus berupa number!'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect() -> route('products.create')->withErrors($validator)->withInput();
        }

        $data = $request->only('name','model', 'price', 'weight');

        if ($request->hasFile('photo')){
            $data['photo'] = $this->savePhoto($request->file('photo'));
        } 

        $product = Product::create($data);
        $product->categories()->sync($request->get('category_lists'));

        Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil menambahkan ".$request->name." kedalam database"]);
        return redirect()->route('products.index');

    }

    public function savePhoto(UploadedFile $photo) {
        $fileName = str_random(40) . '.' . $photo->guessClientExtension();
        $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'img';
        $photo -> move($destinationPath, $fileName);
        return $fileName;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $olddata = Product::find($id);
        $product = Product::findOrFail($id);

        $rules = [
            'name' => 'required|unique:products,name,'. $product->id,
            'model' => 'required',
            'photo' => 'mimes:jpeg,jpg,bmp,png|max:10240',
            'price' => 'required|numeric|min:1000',
            'weight' => 'required|numeric|min:1'
        ];

        $messages = [
            'required' => 'Field harus di isi alias tidak boleh kosong',
            'unique' => 'Nama '.$request -> name.' sudah ada dalam database.',
            'max' => 'Ukuran photo maksimal 10 MB ',
            'mimes' => 'Photo harus berekstensi JPG, JPEG, BMP, atau PNG',
            'numeric' => 'Field harus berupa number!'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect() -> route('products.create')->withErrors($validator)->withInput();
        }

        $data = $request->only('name', 'model', 'price', 'weight');
        if ($request->hasFile('photo')){
            $data['photo'] = $this->savePhoto($request->file('photo'));
            if($product->photo !== '') $this->deletePhoto($product->photo);
        }

        $product->update($data);
        if(count($request->get('category_lists')) > 0){
            $product->categories()->sync($request->get('category_lists'));
        }
        else{
            $product->categories()->detach();
        }

        Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil mengubah ".$olddata->name." menjadi ".$product->name." kedalam database"]);
        return redirect()->route('products.index');
    }

    public function deletePhoto($filename){
        $path = public_path() . DIRECTORY_SEPARATOR . $filename;
        return File::delete($path);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if($product->photo !== '') $this->deletePhoto($product->photo);
        $product->delete();
        Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil meenghapus ".$product->name." dari database"]);
        return redirect()->route('products.index');
    }
}
