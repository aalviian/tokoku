<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Category;
use Validator;
use Session;
use Alert;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {
        $query = $request->get('query');
        $categories = Category::where('title', 'LIKE', '%'.$query.'%')->orderBy('title')->paginate(5);
        return view('categories.index', compact('categories','query'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|string|max:255|unique:categories',
            'parent_id' => 'exists:categories,id'
        ];

        $messages = [
            'required' => 'Field harus di isi alias tidak boleh kosong',
            'unique' => 'Nama '.$request -> title.' sudah ada dalam database.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()) {
            return redirect() -> route('categories.create')->withErrors($validator)->withInput();
        }

        Category::create($request->all());
        Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil menambahkan ".$request->title." kedalam database"]);
        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('categories.edit', compact( 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $olddata = Category::find($id);
        $category = Category::findOrFail($id);

        $rules = [
            'title' => 'required|string|max:255|unique:categories,title,' . $category->id,
            'parent_id' => 'exists:categories,id'
        ];

        $messages = [
            'required' => 'Field harus di isi alias tidak boleh kosong',
            'unique' => 'Nama '.$request -> title.' sudah ada dalam database.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()) {
            return redirect() -> route('categories.create')->withErrors($validator)->withInput();
        }

        $category->update($request->all());

        Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil mengubah ".$olddata->title." menjadi ".$category->title." kedalam database"]);
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        Session::flash('flash_notification', ["level"=>"success", "message"=>"Berhasil meenghapus ".$category->title." dari database"]);
        return redirect()->route('categories.index');
    }
}
