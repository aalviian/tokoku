<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Order;
use App\OrderDetail;
use Session;
use Auth;

class OrdersController extends Controller
{
	public function __construct(){
		$this->middleware('auth');
	}

	public function index(Request $request){
		$status = $request->get('status');
		$orders = Order::where('status', 'LIKE', '%'.$status.'%');
		if ($request->has('q')) {
			$q = $request->get('q');
			$orders = $orders->where(function($query) use ($q) {
				$query->where('id', $q)
					->orWhereHas('user', function($user) use ($q) {
						$user->where('name', 'LIKE', '%'.$q.'%');
					});
			});
		}
		$orders = $orders->paginate(10);
		return view('orders.index', compact('orders', 'status','q'));
	}

	public function edit($id){
		$order = Order::find($id);
		return view('orders.edit')->with(compact('order'));
	}

	public function update(Request $request, $id){
		$order = Order::find($id);

		$this->validate($request, [
		'status' => 'required|in:' . implode(',',Order::allowedStatus())
		]);

		$order->update($request->only('status'));
		
		Session::flash('flash_notification', ["level"=>"success", "message"=>"Order ".$order->padded_id." berhasil disimpan"]);
		return redirect()->route('orders.index');
	} 

	public function orderHistory(){
		$orders = Order::where('user_id', Auth::user()->id)->get();
		return view('orders.order-history', compact('orders'));
	}

	public function orderHistoryDetail(Request $request, $id){
		$details = OrderDetail::where('order_id', $id)->get();
		$order = Order::where('id',$id)->first(); 
		return view('orders.order-history-detail', compact('details','order'));
	}
}
 