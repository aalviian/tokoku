<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Product;
use App\Category;

class CatalogsController extends Controller
{
    public function index(Request $request){
        $query = $request->get('query');
    	if($request->has('category')){
    		$id_category = $request->get('category');
    		$category = Category::findOrFail($id_category);
    		$products = Product::whereIn('id', $category->related_products_id)->where('name','LIKE','%'.$query.'%');
    	}
    	else{
    		$products = Product::where('name','LIKE','%'.$query.'%');
    	}

        if($request->has('sort')){
            $sort = $request->get('sort');
            $order = $request->has('order') ? $request->get('order') : 'asc';
            $field = in_array($sort, ['price', 'name']) ? $request->get('sort') : 'price';
            $products = $products->orderBy($field, $order);
        }

        $products = $products->paginate(4);
    	return view('catalogs.index', compact('products','id_category', 'category','query', 'sort', 'order'));
    }
}
