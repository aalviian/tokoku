<?php
return [
	'bca' => [
		'title' => 'BCA',
		'bank' => 'BCA',
		'name' => 'PT Tokoku',
		'number' => '45454545'
	],
	'bni' => [
		'title' => 'BNI',
		'bank' => 'BNI',
		'name' => 'PT Tokoku',
		'number' => '787687678'
	],
	'mandiri' => [
		'title' => 'Mandiri',
		'bank' => 'Mandiri',
		'name' => 'PT Tokoku',
		'number' => '23223232'
	],
	'atm-bersama' => [
		'title' => 'ATM Bersama',
		'bank' => 'Mandiri',
		'name' => 'PT Tokoku',
		'number' => '23223232'
	],
];